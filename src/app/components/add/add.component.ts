import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { DataService } from '../../services/data.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  user: User = {
    id: 0,
    name: '',
    surname: '',
    company: '',
    jobtitle: '',
    mobile: '',
    email: '',
  };
  length: number = 0;

  constructor(private dataService: DataService, 
              private flashMessages: FlashMessagesService,
              private router: Router) { }

  ngOnInit() {
    this.length = this.dataService.getAll().length + 1;
  }

  onSubmit({value, valid}:{value:User, valid:boolean}){
    value.id = this.length;
    if(!valid){
      this.flashMessages.show("Please fill in the fields", {cssClass:'alert-danger', timeout: 3000});
      this.router.navigate(['/add']);
    } else {
      this.dataService.addUser(value);
      this.flashMessages.show("User Created", {cssClass: 'alert-success', timeout:3000});
      this.router.navigate(['/']);
    }
  }

}

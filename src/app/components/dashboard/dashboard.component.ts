import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  users: any[] = [];
  config: any;
  disableDelete: boolean = false;
  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) {
    this.config = {
      currentPage: 1,
      itemsPerPage: 1
    };

    this.route.queryParams.subscribe((params) => {
      this.config.currentPage = params.page;
    });
  }

  ngOnInit() {
    this.users = this.dataService.getAll();
  }

  pageChange(newPage: number) {
		this.router.navigate([''], { queryParams: { page: newPage } });
  }

  onDelete(){
    if(!this.config.currentPage){
      this.users = this.dataService.deleteUser(this.users, 1);
    } else {
      this.users = this.dataService.deleteUser(this.users, parseInt(this.config.currentPage));
    }

    if(this.users.length === 1){
      this.disableDelete = true;
    }
  }
}

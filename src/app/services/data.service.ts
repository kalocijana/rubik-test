import { Injectable } from '@angular/core';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  collection: User[] = [
    {
      id: 1,
      name: "John",
      surname: "Doe",
      company: "Doe Technologies",
      jobtitle: "Frontend Developer",
      mobile: "0699201977",
      email: "johndoe@gmail.com"
    },
    {
      id: 2,
      name: "Arsen",
      surname: "Cenko",
      company: "Landmark Technologies",
      jobtitle: "Frontend Developer",
      mobile: "0693372720",
      email: "acenko22@gmail.com"
    }
  ];

  constructor() { }

  getAll() {
    return this.collection;
  }

  addUser(user: User){
    this.collection.push(user);
  }

  deleteUser(users: User[], id: number){
    return this.collection = users.filter((obj) => {
      return obj.id !== id;
    });
  }
}

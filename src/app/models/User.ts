export interface User {
    id?: number,
    name?: string,
    surname?: string,
    company?: string,
    jobtitle?: string,
    mobile?: string,
    email?: string
}